<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

$library = dirname(__FILE__) . '/library/XBase.php';
$config = dirname(__FILE__).'/protected/config/main.php';

require_once ($library);

XBase::createWebApplication($config)->run();