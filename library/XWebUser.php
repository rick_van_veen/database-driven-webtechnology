<?php

/**
 * The current web user.
 */
class XWebUser {

    private $_model;
    private $_id;

    function __construct() {
        $this->_id = null;
        $this->_model = null;
        $this->init();
    }

    private function init() {
        XBase::app()->getSession()->open();
        $user_from_session = XBase::app()->getSession()->get('user');
        if ($user_from_session != null) { // restore session.
            $this->_id = $user_from_session;
        }
    }

    private function getModel() {
        if (!isset($this->_id)) {
            $this->_model = new User();
        }
        if ($this->_model === null) {
            $this->_model = new User ();
            $this->_model->loadModel($this->_id);
        }
        return $this->_model;
    }

    public function __get($name) {
        $m = $this->getModel();
        if ($m->__isset($name))
            return $m->{$name};
    }

    public function __call($name, $parameters) {
        $m = $this->getModel();
        return call_user_func_array(array($m, $name), $parameters);
    }

    public function __set($name, $value) {
        $m = $this->getModel();
        $m->{$name} = $value;
    }

    public function login($identity, $duration = 0) {
        if ($identity === null)
            throw new XException("Identity can not be null.");

        $this->_id = $identity->get_id();
        
        XBase::app()->getSession()->add('user', $this->_id);
        XBase::app()->getSession()->regenerateId(true);
    }

    public function logout() {
        $this->_id = null;
        XBase::app()->getSession()->destroy();
        //probably set some stuff in a session....
    }

    public function isGuest() {
        return $this->_id == null;
    }

    // id values: user->id, project->owner_id.
    public function isOwner($id) {
        if (isset($this->_id))
            return $this->_id == $id;
        return false;
    }

    public function getId() {
        return $this->_id;
    }

}
