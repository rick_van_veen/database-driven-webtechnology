<?php

/**
 *  Define this directory as the root directory.
 */
defined('XBASE_PATH') or define('XBASE_PATH', dirname(dirname(__FILE__)));
/**
 *  Controller path
 */
defined('CONTROLLER_PATH') or define('CONTROLLER_PATH', XBASE_PATH . '/protected/controllers/');
/**
 *  Model path
 */
defined('MODEL_PATH') or define('MODEL_PATH', XBASE_PATH . '/protected/models/');
/**
 *  View path
 */
defined('VIEW_PATH') or define('VIEW_PATH', XBASE_PATH . '/protected/views/');

/**
 * XBase is a helper class serving common ``framework'' functionalities.
 */
Class XBase {

    /**
     * The variable where the webapplication is stored. To access this variable 
     * use {@see app()}
     * 
     * @var XWebApplication
     */
    private static $_app;

    /**
     * The singleton database object.
     * 
     * @var XDatabase 
     */
    private static $_db;

    /**
     * This function will create a webapplication. {@see XApplication}
     * 
     * @return {@see XApplication}
     */
    public static function createWebApplication($config) {
        return self::createApplication('XWebApplication', $config);
    }

    /**
     * Will create an application
     * 
     * @param string $class
     * @param string $config
     * @return $class type class
     */
    private static function createApplication($class, $config) {
        return self::$_app = new $class($config);
    }

    /**
     * This function makes sure there will be only one instance of an application.
     * 
     * @param XWebApplication $app is the application that needs to be registered.
     * @throws XException if multiple instances are registered. 
     */
    public static function setApplication($app) {
        if (self::$_app === null || $app === null)
            self::$_app = $app;
        else
            throw new XException('XBase: Application can only be created once!');
    }

    /**
     * This function makes sure  ther will be only one instance of an database.
     * 
     * @param XDatabase $db the XDatabase object
     * @throws XException when there is already a db connection set.
     */
    public static function setDatabase($db) {
        if (self::$_db === null || $db === null)
            self::$_db = $db;
        else
            throw new XException('XBase: DatabaseConnection can only be created once!');
    }

    /**
     * With this function you can access the webapplication singleton.
     * 
     * @return XWebApplication is the singleton application object.
     */
    public static function app() {
        return self::$_app;
    }

    /**
     * With this function you can access the XDatabase singleton object.
     * 
     * @return XDatabase
     */
    public static function db() {
        return self::$_db;
    }

    /**
     * This function is set as the autoload function so there is no need
     * to put an import above all the classes.
     * 
     * @param string $class is the name of the class that should be imported
     * @throws XException when class file couldn't be found
     */
    public static function __autoload($class) {
        $class_file = $class . '.php';
        if (array_key_exists($class, self::$core_class_map))
            require_once(XBASE_PATH . self::$core_class_map[$class]);
        else if (file_exists(CONTROLLER_PATH . $class_file))
            require_once(CONTROLLER_PATH . $class_file);
        else if (file_exists(MODEL_PATH . $class_file))
            require_once(MODEL_PATH . $class_file);
        else
            throw new XException('XBase: Autoload function coudn\'t find class: ' . $class . '.');
    }

    /**
     * Array holds all the paths to the library classes. 
     * @var array &core_class_map 
     */
    private static $core_class_map = array(
        'XWebApplication' => '/library/XWebApplication.php',
        'XException' => '/library/XException.php',
        'XBaseController' => '/library/XBaseController.php',
        'XExceptionHandler' => '/library/XExceptionHandler.php',
        'XDatabase' => '/library/XDatabase.php',
        'XBaseModel' => '/library/XBaseModel.php',
        'UserIdentity' => '/library/components/UserIdentity.php',
        'XWebUser' => '/library/XWebUser.php',
        'XSession' => '/library/XSession.php',
        'XPasswordHash' => '/library/XPasswordHash.php',
        'XSecurityManager' => '/library/XSecurityManager.php',
        'XValidator' => '/library/XValidator.php',
        'XHttpException' => '/library/XHttpException.php',
    );

}

/**
 * This function has te be called in order to register the function that will auto load the needed classes. 
 */
spl_autoload_register(array('XBase', '__autoload'));