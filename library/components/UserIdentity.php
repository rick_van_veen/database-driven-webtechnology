<?php

/**
 * Class that represents an identity of an web user.
 */
class UserIdentity {

    private $_email;
    private $_password;
    private $_id;
    private $_user;

    function __construct($email, $password) {
        $this->_email = $email;
        $this->_password = $password;
        $this->_user = new User();
    }

    public function authenticate() {
        $user = $this->_user->find(array('email' => $this->_email));

        if ($user !== null) { // User with $this->_email exists.
            $this->_user->setAttributes($user);
            //unset($user);
            if (XSecurityManager::verifyPassword($this->_password, $this->_user->password)) {
                $this->_id = $this->_user->id;
                return true;
            }
        }
        return false;
    }

    public function get_id() {
        return $this->_id;
    }

}
