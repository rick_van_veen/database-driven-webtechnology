<?php

class XSession {

    private $_lifetime;

    function __construct($lifetime = 5000) {
        $this->init();
        $this->_lifetime = $lifetime;
    }

    private function init() {
        $this->setSessionName('TheBigIdea');
    }

    public function open() {
        session_start();
        if ($this->get('expiration') != null) {
            if (time() > $this->get('expiration')) {
                $this->destroy();
                $this->open();
            }
        } else {
            $this->add('expiration', (time() + $this->_lifetime));
        }
    }

    public function close() {
        if (session_id() !== '')
            session_write_close();
    }

    public function destroy() {
        session_destroy();
    }

    public function regenerateId($delete_old_session = false) {
        session_regenerate_id($delete_old_session);
    }

    public function getSessionId() {
        return session_id();
    }

    public function setSessionName($value) {
        session_name($value);
    }

    public function getSessionName() {
        return session_name();
    }

    public function setSessionId($value) {
        session_id($value);
    }

    public function getKeys() {
        return array_keys($_SESSION);
    }

    public function get($key, $defaultValue = null) {
        return isset($_SESSION[$key]) ? $_SESSION[$key] : $defaultValue;
    }

    public function add($key, $value) {
        $_SESSION[$key] = $value;
    }

    public function remove($key) {
        if (isset($_SESSION[$key])) {
            $value = $_SESSION[$key];
            unset($_SESSION[$key]);
            return $value;
        }
        else
            return null;
    }

    public function clear() {
        foreach (array_keys($_SESSION) as $key)
            unset($_SESSION[$key]);
    }

}