<?php

/**
 * HTML class for outputting html in the php code. Not finished and not used.
 */
class XHtml {

    public static function tag($tag, $content = '', $tabs = 0, $html_options = array()) {
        $options_string = empty($html_options) ? '' : ' ' . self::htmlOptionsToString($html_options);
        return str_repeat("    ", $tabs) . '<' . $tag . $options_string . '>' . $content . '</' . $tag . '>' . "\n";
    }

    public static function openTag($tag, $tabs = 0, $html_options = array()) {
        $options_string = empty($html_options) ? '' : ' ' . self::htmlOptionsToString($html_options);
        return str_repeat("    ", $tabs) . '<' . $tag . $options_string . '>' . "\n";
    }

    public static function closeTag($tag, $tabs = 0) {
        return '</' . $tag . '>' . "\n";
    }

    private static function htmlOptionsToString($html_options) {
        $string = '';
        foreach ($html_options as $key => $value) {
            $string .= $key . '="' . $value . '"';
        }
        return $string;
    }

}