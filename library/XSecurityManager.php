<?php

/**
 * Class where we can put all things that have to do with security. Password hashing functions, hash generators etc...
 */
class XSecurityManager {

    private static $hasher;

    private static function initHasher($cost = 13) {
        self::$hasher = new XPasswordHash($cost, false);
    }

    public static function hashPassword($password, $cost = 13) {
        if (self::$hasher === null)
            self::initHasher($cost);

        $password_hash = self::$hasher->HashPassword($password);
        return $password_hash;
    }

    public static function verifyPassword($password, $password_hash) {
        if (self::$hasher === null)
            self::initHasher();

        return self::$hasher->CheckPassword($password, $password_hash);
    }

    public static function same($str1, $str2) {
        return (strcmp($str1, $str2) == 0);
    }

}
