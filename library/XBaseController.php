<?php

/**
 * 
 */
abstract class XBaseController {

    /**
     * The layout file
     * @var file 
     */
    protected $_layout = '/layouts/main.php';

    /**
     * This is the action performed if no action is set. 
     * @var string $_default_action 
     */
    private $_default_action = 'index';

    /**
     * This is the name of this controller.
     * @var string $_controller_name 
     */
    private $_controller_name;

    /**
     * The path where the views of this controller are stored. 
     * @var string $_view_path 
     */
    private $_view_path;

    // The two actions every controller needs.
    public abstract function actionIndex();

    public abstract function actionError();

    /**
     * Constructor which will set the controller name and view path.
     * 
     * @param string $controller_name name of this controller. 
     * This is needed for the {@see $_view_path}
     */
    function __construct($view_directory) {
        $this->_controller_name = get_class($this);
        $this->_view_path = $this->createViewPath($view_directory);
    }

    /**
     * Creates the path to the view directory.
     * 
     * @return string the path to the view directory.
     */
    private function createViewPath($view_directory) {
        return VIEW_PATH . $view_directory . '/';
    }

    /**
     * Gets the full path and filename to the view file named $view.
     * 
     * @param string $view is the name of the file.
     * @return string path to a certain view file in the view path.
     */
    private function getViewFile($view) {
        $file = $this->_view_path . $view . '.php';
        return file_exists($file) ? $file : false;
    }

    /**
     * Will return the path and file of the layout.
     * 
     * @return string 
     */
    private function getLayoutFile() {
        return VIEW_PATH . $this->_layout;
    }

    /**
     * Renders the file with the data but does not use the layout.
     * 
     * @param file $view
     * @param mixed $data
     * @return string 
     * @throws XException if it cant find the requested view.
     */
    public function renderPartial($view, $data = null) {
        // Create a view_file name path/views/$view.php
        if (($view_file = $this->getViewFile($view)) !== false) {
            return $this->renderFile($view_file, $data);
        }
        else
            throw new XException($this->_controller_name . ' cannot find the requested view "' . $view . '".');
    }

    /**
     * Renders a view through the layout.
     * 
     * @param string $view is the view that needs to be rendered.
     */
    public function render($view, $data = null) {
        $output = $this->renderPartial($view, $data);

        if (($layout = $this->getLayoutFile()) !== false) {
            echo $this->renderFile($layout, array('content' => $output));
        }
    }

    /**
     * ``Magical'' function that does something with putting everything into 
     * a buffer and only writing it when all files are rendered.
     * 
     * @param string $file
     * @param array $data
     * @return string
     */
    private function renderFile($file, $data = null) {
        if (is_array($data) && !empty($data))
            extract($data);

        ob_start(); // Puts the output buffering on.
        include $file;
        return ob_get_clean(); // Gets the content and cleans the buffer.
    }

    /**
     * Returns the default action.
     * 
     * @return string the default action {@see $_default_action}
     */
    public function defaultAction() {
        return $this->_default_action;
    }

    /**
     * Function to redirect the user.
     * 
     * @param string $url
     */
    public function redirect($url) {
        // @todo Create url function. 
        header('Location: index.php?r=' . $url);
    }

}
