<?php

class XDatabase {

    private $_connection;

    function __construct($db_config) {
        XBase::setDatabase($this);
        $this->initDb($db_config);
    }

    private function initDb($db_config) {
        $this->_connection = new PDO($db_config['dsn'], $db_config['username'], $db_config['password']);
        $this->_connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        $this->_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    /**
     * @todo doe fixen some queries wont have output, so need something to fix this.
     * @param type $sql_string
     * @param type $var_array
     * @return boolean
     */
    public function query($sql_string, $fetch = true, $var_array = array()) {
        if (!is_array($var_array))
            throw new XException('Query function needs the third argument to be an array.');

        $sql_query = $this->_connection->prepare($sql_string);
        $result = null;
        if ($sql_query->execute($var_array))
            $result = $sql_query;

        return $fetch ? $result->fetchAll() : $result;
    }

    public function getMetaData($table) {
        $meta_data = array();

        $query = $this->query('SELECT * FROM ' . $table, false);
        $count = $query->columnCount();

        for ($i = 0; $i < $count; $i++) {
            $column_meta = $query->getColumnMeta($i);
            if (key_exists('name', $column_meta)) {
                $key = $column_meta['name'];
                $meta_data[$key] = array();
                if (key_exists('flags', $column_meta))
                    $meta_data[$key]['flags'] = $column_meta['flags'];
            }
        }
        return $meta_data;
    }

    private function buildValues($values, $size) {
        $sql_string = '(';
        for ($i = 0; $i < $size; $i++) {
            if ($i == $size - 1)
                $sql_string .= ' ? )';
            else
                $sql_string .= ' ? ,';
        }
        return $sql_string;
    }

    private function buildColumns($columns, $size) {
        $sql_string = '(';
        $columns_keys = array_keys($columns);
        for ($i = 0; $i < $size; $i++) {
            if ($i == $size - 1)
                $sql_string .= $columns_keys[$i] . ')';
            else
                $sql_string .= $columns_keys[$i] . ', ';
        }
        return $sql_string;
    }

    public function buildInsertQuery($table, $attributes) {
        $size = count($attributes);
        $sql_string = 'INSERT INTO ' . $table . ' ' .
                $this->buildColumns($attributes, $size) .
                ' VALUES ' .
                $this->buildValues($attributes, $size);

        return $sql_string;
    }

    private function buildColumnValues($columns, $size) {
        $sql_string = '';
        $i = 0;
        foreach ($columns as $key => $value) {
            if ($i == $size - 1)
                $sql_string .= $key . ' = ? ';
            else
                $sql_string .= $key . ' = ?, ';
            $i++;
        }
        return $sql_string;
    }

    public function buildUpdateQuery($table, $pk, $attributes) {
        $size = count($attributes);
        $sql_string = 'UPDATE ' . $table . ' SET ' .
                $this->buildColumnValues($attributes, $size) .
                'WHERE ' . $pk . ' = ? ';

        return $sql_string;
    }

    public function buildFindQuery($table, $attributes) {
        $size = count($attributes);
        $sql_string = 'SELECT * FROM ' . $table .
                ' WHERE ' . $this->buildColumnValues($attributes, $size);

        return $sql_string;
    }

    public function getLastInsertedRow() {
        return $this->_connection->lastInsertId();
    }

}