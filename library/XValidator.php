<?php

class XValidator {

    /**
     * Validates the a date of format (yyyy-mm-dd)
     * 
     * @param string $date the date that should be validated
     */
    public static function validateDate($date, $format = 'Y-m-d') {
        if (!self::isEmpty($date)) {
            $d = DateTime::createFromFormat($format, $date);
            return $d && $d->format($format) == $date;
        }
        return false;
    }

    /**
     * Check if something is empty.
     * 
     * @param mixed $field
     * @return boolean
     */
    public static function isEmpty($field) {
        return empty($field);
    }

    /**
     * Checks if a given string is an email.
     * 
     * @param string $email
     * @return boolean
     */
    public static function validateEmail($email) {
        if (!self::isEmpty($email))
            return !filter_var($email, FILTER_VALIDATE_EMAIL);

        return false;
    }

}
