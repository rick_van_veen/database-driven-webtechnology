<?php

/**
 * The main application class.
 * 
 */
class XWebApplication {

    /**
     * This is the class name of the ExceptionHandler
     * 
     * NOTE: Maybe we dont realy need this, but if you would ever use this 
     * framework again, you could use some setter to let this variable be changed 
     * to an user defined class. 
     * 
     * @var string 
     */
    private $exception_handler_class = 'XExceptionHandler';

    /**
     * Array with all configurable components stored as key => values 
     * @var array 
     */
    private $_config;

    /**
     * The current webuser.
     * @var XWebUser 
     */
    private $_user;

    /**
     * The session 
     * @var XSession 
     */
    private $_session;

    /**
     * Constructor calls the XBase {@see setApplication()} to register itself as the application.
     * @param file $config
     */
    function __construct($config = null) {
        XBase::setApplication($this);
        $this->initExceptionHandler();
        $this->init($config);
        $this->initDatabase();
    }

    /**
     * Init function that reads the config file. The data is then stored in the
     * config variable in this class.
     *  
     * @param file $config is a path to the config not an array
     * @throws XException when no config path/file is given or the given file 
     * does not exist.
     */
    private function init($config) {
        if (file_exists($config)) {
            $this->_config = require_once($config);
            if (empty($this->_config))
                throw new XException("Need a config file.");
        }
        else
            throw new XException("Config file does not exist.");
    }

    /**
     *  Sets the default exception handler to the exception_handler function of
     *  XBase. All uncaught exceptions will go here.
     */
    private function initExceptionHandler() {
        set_exception_handler(array($this->exception_handler_class, 'handleException'));
    }

    /**
     * Init database funtion will create a database connection which can be used
     * by the application. 
     * 
     * NOTE: It is not possible to not have a db, in this version.
     * 
     * @throws XException when the db component in the config is not set.
     */
    private function initDatabase() {
        if (($db_config = $this->getConfig('db')) !== false)
            new XDatabase($db_config);
        else
            throw new XException("Could not find config for the database.");
    }

    /**
     * This function will look for the config component called $key
     * 
     * @param string $key the component we are looking for.
     * @return mixed the key value linked to the given key or FALSE otherwise.
     */
    private function getConfig($key) {
        if (key_exists($key, $this->_config))
            return $this->_config[$key];
        else
            return false;
    }

    /**
     * Will return the current XWebUser object or will create a new empty one.
     * @return XWebUser
     */
    public function getUser() {
        if ($this->_user == null) {
            $this->_user = new XWebUser();
        }
        return $this->_user;
    }

    /**
     * Will return a new empty XSession or the existing one.
     * @return XSession
     */
    public function getSession() {
        if ($this->_session === null) {
            $this->_session = new XSession();
        }
        return $this->_session;
    }

    /**
     * Helper function to create a controller name. Used in the run() function to 
     * parse ``site'' to ``SiteController''.
     * 
     * @param string $controller_name is the name of the controller e.g. ``site''
     * @return string the controllername so it can be loaded e.g. ``SiteController''
     */
    private function createControllerName($controller_name) {
        return ucfirst($controller_name) . 'Controller';
    }

    /**
     * Helper function to create an action name. Used in the run() function to
     * parse ``index'' to ``actionIndex''.
     * 
     * @param string $action_name is the name of the action e.g. ``index''
     * @return string the action name so it can be called e.g. ``actionIndex''
     */
    private function createActionName($action_name) {
        return 'action' . ucfirst($action_name);
    }

    /**
     * Will return the GET variable ``r'' which stores the path to the 
     * controller action.
     * 
     * @return string is the route set in the url bar, will return a default 
     * route if r is not set.
     */
    public function getRoute() {
        if (isset($_GET['r']))
            return $_GET['r'];

        // @todo probably move this to the config file.
        return 'site/index';
    }

    /**
     * The main function of the XApplication is to parse the {@see getRoute()} 
     * and to create the controller so the the action in the route can be called.
     * Typical route: http:://www.website.com/index.php?r=user/update/2.
     * This should make a UserController instance and call {@see call_user_func_array()} 
     * the action actionUpdate if it exists {@see method_exists()}
     * with the parameter 2. Every parameter should be checked by the method in
     * the controller.
     */
    public function run() {
        // Typical route: controller/action/param1/param.../paramN
        $url_array = explode("/", $this->getRoute());

        // Processing controller part of the url and creating a new instance of that controller . e.g. site -> SiteController
        $url_controller_name = array_shift($url_array);
        $controller_name = $this->createControllerName($url_controller_name);
        $controller_obj = new $controller_name($url_controller_name);

        // Processing action part of the url if there is one.
        $url_action_name = array_shift($url_array);
        if ($url_action_name === null)
            $action_name = $this->createActionName(call_user_func(array($controller_obj, defaultAction())));
        else
            $action_name = $this->createActionName($url_action_name);

        if (!method_exists($controller_obj, $action_name))
            $action_name = $this->createActionName('error');

        // The parameters are put in an array. Every param should be checked by every method.
        call_user_func_array(array($controller_obj, $action_name), array($url_array));
    }

}
