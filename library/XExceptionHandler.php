<?php

class XExceptionHandler {

    /**
     * This function will catch all uncaught exceptions.
     * 
     * @param Exception $exception is the exception that is thrown but not caught.
     */
    public static function handleException($exception) {
        $exception_type = get_class($exception);
        if ($exception_type == 'XHttpException') {
            
        } else {
            echo "Uncaught exception in class " . $exception->getFile() . " " . $exception->getMessage();
        }
    }

}
