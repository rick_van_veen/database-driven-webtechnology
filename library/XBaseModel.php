<?php

/**
 * 
 */
class XBaseModel {

    protected $_table;
    protected $_attributes;
    protected $_meta_data;
    protected $_primary_key;
    protected $_db;
    protected $_new_record;
    public $errors;

    //protected $_rules; database validation rules. Derive from metadata?

    function __construct() {
        if (!isset($this->_table))
            throw new XException("You first need to set the table for this model");

        $this->_attributes = array();
        $this->_db = XBase::db();
        $this->_meta_data = $this->_db->getMetaData($this->_table);
        $this->_primary_key = $this->getPrimaryKey();
        $this->_new_record = true;
        $this->errors = array();
    }

    public function __get($name) {
        if ($this->__isset($name))
            return $this->_attributes[$name];

        throw new XException('Unknown or not set attribute: ' . $name);
    }

    public function __set($name, $value) {
        if (key_exists($name, $this->_attributes))
            $this->_attributes[$name] = $value;
        else
            throw new XException('Attribute "' . $name . '" does not exist.');
    }

    public function __isset($key) {
        return key_exists($key, $this->_attributes);
    }

    protected function setTable($table) {
        $this->_table = $table;
    }

    protected function getAttributes() {
        foreach ($this->_meta_data as $key => $value) {
            $this->_attributes[$key] = null;
        }
    }

    public function setAttributes($attr_array) {
        if (is_array($attr_array)) {
            foreach ($attr_array as $key => $value) {
                if ($this->__isset($key))
                    $this->$key = $value;
            }
        }
        else
            throw new XException("Function set_attributes needs an array as argument.");
    }

    protected function getPrimaryKey() {
        foreach ($this->_meta_data as $column_name => $column_attributes) {
            foreach ($column_attributes['flags'] as $value) {
                if ($value === 'primary_key')
                    return $column_name;
            }
        }
    }

    public function hasError($attribute) {
        return key_exists($attribute, $this->errors);
    }

    public function hasErrors() {
        return !empty($this->errors);
    }

    public function find($attributes) {
        if (!is_array($attributes))
            throw new XException("Function needs array as argument.");

        $sql_string = $this->_db->buildFindQuery($this->_table, $attributes);

        $result = $this->_db->query($sql_string, true, array_values($attributes));

        return !empty($result) ? $result[0] : null;
    }

    public function findAll($fetch = false) {
        $sql_string = 'SELECT * FROM ' . $this->_table;
        return $this->_db->query($sql_string, $fetch);
    }

    public function loadModel($id) {
        $model = $this->_db->query("SELECT * FROM " . $this->_table . " WHERE " . $this->_primary_key . " = ?", true, array($id));
        if (count($model) == 1) {
            $this->setAttributes($model[0]);
            $this->_new_record = false;
        } else {
            throw new XException("Multiple models with same pk...");
        }
    }

    public function isNewRecord() {
        return $this->_new_record;
    }

    public function save() {
        // @todo verification before saving and partial save?
        if ($this->_new_record) {
            $sql_string = $this->_db->buildInsertQuery($this->_table, $this->_attributes);

            $this->_db->query($sql_string, false, array_values($this->_attributes));
            $this->id = ($this->_db->getLastInsertedRow()); // NOTE: For this to work all models need to have a id field. I think.
        } else {
            // Copy the attributes array and remove primary_key from array.
            $attributes_copy = $this->_attributes;
            unset($attributes_copy[$this->_primary_key]);

            // Build the query
            $sql_string = $this->_db->buildUpdateQuery($this->_table, $this->_primary_key, $attributes_copy);

            // Get the value of the pk.
            $pk_value = $this->_attributes[$this->_primary_key];

            // Change it to an array of values without keys and add the value of the pk at the end.
            $attributes_copy = array_values($attributes_copy);
            $attributes_copy[] = $pk_value;

            $this->_db->query($sql_string, true, $attributes_copy);
        }
    }

}
