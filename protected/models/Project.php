<?php

//

class Project extends XBaseModel {

    function __construct() {
        $this->setTable("projects");
        parent ::__construct($this);
        $this->getAttributes();
    }

    function validate() {
        // You want to get all the errors I think. So you do not need to return after an error was found.
        if (XValidator::isEmpty($this->title)) {
            $this->errors['title'] = "Please enter a title";
        }
        if (XValidator::isEmpty($this->category_id)) {
            $this->errors['category_id'] = "Please choose a category";
        }
        if (XValidator::isEmpty($this->description)) {
            $this->errors['description'] = "Please enter a description";
        }
        if (XValidator::isEmpty($this->begin_date)) {
            $this->errors['begin_date'] = "Please enter a begin date";
        }
        if (XValidator::isEmpty($this->end_date)) {
            $this->errors['end_date'] = "Please enter an end date";
        }

        $this->compare_dates($this->begin_date, $this->end_date);

        return !$this->hasErrors();
    }

    /* compares the date so begin date can not be later than end date     */

    function compare_dates($begin_date, $end_date) {
        $begin = date("Y-m-d", strtotime($this->begin_date));
        $end = date("Y-m-d", strtotime($this->end_date));

        $date1 = new DateTime($begin);
        $date2 = new DateTime($end);

        if ($date1->format('%y') == $date2->format('%y')) {
            if ($date1->format('%m') == $date2->format('%m')) {
                if ($date1->format('%d') > $date2->format('%d')) {
                    $this->errors['end_date'] = "Begin date can not be later than end date";
                    return false;
                }
            } else if ($date1->format('%m') > $date2->format('%m')) {
                $this->errors['end_date'] = "Begin date can not be later than end date";
                return false;
            }
        } else if ($date1->format('%y') > $date2->format('%y')) {
            $this->errors['end_date'] = "Begin date can not be later than end date";
            return false;
        }
        return true;
    }

}

?>