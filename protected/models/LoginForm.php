<?php

class LoginForm {

    public $email;
    public $password;
    public $rememberMe;
    public $errors;

    /**
     * This should be some class that handles the authentication and stuff.
     * @var Identityclass
     */
    private $_identity;

    function __construct() {
        $this->email = null;
        $this->password = null;
        $this->rememberMe = null;
        $this->errors = array();
        $this->_identity = null;
    }

    private function validateEmail() {
        if (XValidator::isEmpty($this->email))
            $this->errors['email'] = 'Email can not be empty.';
        else { // if not empty check if it is a valid email address.
            if (XValidator::validateEmail($this->email))
                $this->errors['email'] = 'Email needs to be a valid email address.';
        }
    }

    private function validatePassword() {
        if (XValidator::isEmpty($this->password))
            $this->errors['password'] = 'Password can not be empty.';
    }

    private function validateRememberMe() {
        return;
    }

    public function validate() {
        $this->validateEmail();
        $this->validatePassword();
        $this->validateRememberMe();

        return !$this->hasErrors();
    }

    public function hasError($attribute) {
        return key_exists($attribute, $this->errors);
    }

    public function hasErrors() {
        return !empty($this->errors);
    }

    public function unsetAttributes() {
        $this->email = '';
        $this->password = '';
    }

    public function login($duration = 0) {
        if ($this->_identity === null) {
            $this->_identity = new UserIdentity($this->email, $this->password);
        }
        if ($this->_identity->authenticate()) {
            XBase::app()->getUser()->login($this->_identity, $duration);
            return true;
        }
        $this->errors['authentication'] = 'Unknown user credentials';
        return false;
    }

}
