<?php

class User extends XBaseModel {

    public $password_repeat;

    function __construct() {
        $this->setTable("users");
        parent::__construct();
        $this->getAttributes();
    }

    public function validate() {
        /* #### first_name #### */
        if (XValidator::isEmpty($this->first_name))
            $this->errors['first_name'] = "First name can not be blank.";

        /* #### last_name #### */
        if (XValidator::isEmpty($this->last_name))
            $this->errors['last_name'] = "Last name can not be blank.";

        /* #### birth_date #### */
        if (XValidator::isEmpty($this->birth_date))
            $this->errors['birth_date'] = "Birth date can not be blank.";
        else if (!XValidator::validateDate($this->birth_date))
            $this->errors['birth_date'] = "Birth date needs the format yyyy-mm-dd";

        /* #### gender #### */
        if (XValidator::isEmpty($this->gender))
            $this->errors['gender'] = "Gender can not be blank.";

        /* #### country #### */
        if (XValidator::isEmpty($this->country))
            $this->errors['country'] = "Country can not be blank.";

        /* #### email #### */
        if (XValidator::isEmpty($this->email))
            $this->errors['email'] = "Email can not be blank.";
        else if (XValidator::validateEmail($this->email))
            $this->errors['email'] = 'Email needs to be a valid email address.';
        else if ($this->find(array('email' => $this->email)) != null)
            $this->errors['email'] = 'Email already taken';

        /* #### password and password_repeat #### */
        if (XValidator::isEmpty($this->password))
            $this->errors['password'] = "Password can not be blank.";

        if (XValidator::isEmpty($this->password_repeat))
            $this->errors['password_repeat'] = "Password repeat can not be blank.";

        if (!$this->hasError('password') && !$this->hasError('password_repeat'))
            if (!XSecurityManager::same($this->password, $this->password_repeat)) {
                $this->errors['password'] = "Password must be the same as password repeat.";
                $this->errors['password_repeat'] = "Password repeat must be the same as password.";
            }

        return !$this->hasErrors();
    }

}
