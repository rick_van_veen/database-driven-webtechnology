SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `webtech` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `webtech` ;

-- -----------------------------------------------------
-- Table `webtech`.`users`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `webtech`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT 'User id' ,
  `first_name` VARCHAR(32) NOT NULL COMMENT 'First name' ,
  `last_name` VARCHAR(32) NOT NULL COMMENT 'Last name' ,
  `birth_date` DATE NOT NULL COMMENT 'Birth date' ,
  `gender` CHAR(1) NOT NULL COMMENT 'Gender: {m, f}' ,
  `country` VARCHAR(64) NOT NULL COMMENT 'Country' ,
  `email` VARCHAR(64) NOT NULL COMMENT 'Email' ,
  `password` VARCHAR(128) NOT NULL COMMENT 'Password' ,
  `registration_date` DATETIME NOT NULL COMMENT 'Registration date' ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `webtech`.`categories`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `webtech`.`categories` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT 'Category id' ,
  `title` VARCHAR(32) NOT NULL COMMENT 'Title' ,
  `description` TEXT NULL COMMENT 'Description' ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `name_UNIQUE` (`title` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `webtech`.`projects`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `webtech`.`projects` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT 'Project id' ,
  `title` VARCHAR(32) NOT NULL COMMENT 'Title' ,
  `description` TEXT NOT NULL COMMENT 'Description' ,
  `create_time` DATETIME NOT NULL COMMENT 'Create time' ,
  `begin_date` DATETIME NOT NULL ,
  `end_date` DATETIME NULL ,
  `completed` TINYINT(1) NOT NULL ,
  `category_id` INT NOT NULL ,
  `owner_id` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `project_category_rel_idx` (`category_id` ASC) ,
  INDEX `project_user_rel_idx` (`owner_id` ASC) ,
  CONSTRAINT `project_category_rel`
    FOREIGN KEY (`category_id` )
    REFERENCES `webtech`.`categories` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `project_user_rel`
    FOREIGN KEY (`owner_id` )
    REFERENCES `webtech`.`users` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `webtech`.`tasks`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `webtech`.`tasks` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT 'Task id' ,
  `description` TEXT NULL COMMENT 'Description' ,
  `question` TEXT NOT NULL COMMENT 'Question' ,
  `question_type` INT NOT NULL COMMENT 'Question type' ,
  `answer_string` TEXT NULL COMMENT 'Answer' ,
  `completed` TINYINT(1) NOT NULL COMMENT 'True if task is completed' ,
  `project_id` INT NOT NULL COMMENT 'Project id' ,
  PRIMARY KEY (`id`) ,
  INDEX `task_project_rel_idx` (`project_id` ASC) ,
  CONSTRAINT `task_project_rel`
    FOREIGN KEY (`project_id` )
    REFERENCES `webtech`.`projects` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `webtech`.`completed_tasks`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `webtech`.`completed_tasks` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `user_id` INT NOT NULL ,
  `task_id` INT NOT NULL ,
  `completion_time` DATETIME NOT NULL ,
  `anonymous` TINYINT(1) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `completed_tasks_user_rel_idx` (`user_id` ASC) ,
  INDEX `completed_tasks_task_rel_idx` (`task_id` ASC) ,
  CONSTRAINT `completed_tasks_user_rel`
    FOREIGN KEY (`user_id` )
    REFERENCES `webtech`.`users` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `completed_tasks_task_rel`
    FOREIGN KEY (`task_id` )
    REFERENCES `webtech`.`tasks` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `webtech`.`achievements`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `webtech`.`achievements` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `title` VARCHAR(32) NOT NULL ,
  `description` VARCHAR(128) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `webtech`.`earned_achievements`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `webtech`.`earned_achievements` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `user_id` INT NOT NULL ,
  `achievement_id` INT NOT NULL ,
  `awarded_date` DATE NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `achievement_user_rel_idx` (`user_id` ASC) ,
  INDEX `achievement_achievement_rel_idx` (`achievement_id` ASC) ,
  CONSTRAINT `achievement_user_rel`
    FOREIGN KEY (`user_id` )
    REFERENCES `webtech`.`users` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `achievement_achievement_rel`
    FOREIGN KEY (`achievement_id` )
    REFERENCES `webtech`.`achievements` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `webtech` ;

CREATE USER 'webtech' IDENTIFIED BY 'webtech';

GRANT SELECT, INSERT, TRIGGER, UPDATE, DELETE ON TABLE `webtech`.* TO 'webtech';

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
