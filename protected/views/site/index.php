<div class="page-header">
    <h1>What is crowsourcing</h1>
</div>

<p class="lead">
    Crowdsourcing is the practice of obtaining needed services, ideas, or content
    by soliciting contributions from a large group of people, and especially 
    from an online community, rather than from traditional employees or suppliers. 
    It combines the efforts of numerous self-identified volunteers or part-time 
    workers, where each contributor of their own initiative adds a small portion 
    to the greater result.
</p>

<p>
    The principle of crowdsourcing is that more heads are better than one. 
    By canvassing a large crowd of people for ideas, skills, or participation, 
    the quality of content and idea generation will be superior.
</p>

<p>
    Crowdsourcing&rsquo;s biggest benefit is the ability to receive better 
    quality results, since several people offer their best ideas, skills and 
    support. Crowdsourcing allows you to select the best result from a sea of 
    best entries as opposed to receiving the best entry from a single provider.
</p>

<h2>Bring creative projects to life!</h2>

<hr>
<p>
    People ,who have a creative idea , upload their projects to system for the 
    purpose of pre-searching to get idea about&nbsp; it&rsquo;s future. By using
    the ratings, they will be able to have an idea about the market research.&nbsp;
</p>

<h2>Join us</h2>
<hr>
<p>
    If you have a creative idea , it&rsquo;s enough to sign up . In this way, you 
    will be able to have an account, upload your project here or vote other 
    user&rsquo;s projects. You will win awarding points for every tasks completed
    and that will be shown on your profile. And there will be top users list where 
    people can see.
</p>

<p>
    Also, if you just want to vote and don&rsquo;t have a project idea, of course
    you can vote the other projects but you can&rsquo;t be awarded any achievements.
</p>