<?php
/* @var $model LoginForm */
?>

<?php
if ($model->hasError('authentication'))
    echo '<div class="alert alert-danger">' . $model->errors['authentication'] . '</div>';
?>
<form role="form" id="input-form" class="col-lg-6" action="index.php?r=site/login" method="post">
    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <div class="form-group <?php echo $model->hasError('email') ? 'has-error' : ''; ?>">
        <label class="control-label" for="LoginForm_email">Email <span class="required">*</span></label>
        <input class="form-control" name="LoginForm[email]" id="LoginForm_email" type="email" value="<?php echo $model->email ?>"/>
        <?php
        if ($model->hasError('email'))
            echo '<div class="alert alert-danger">' . $model->errors['email'] . '</div>';
        ?>
    </div>

    <div class="form-group <?php echo $model->hasError('password') ? 'has-error' : ''; ?>">
        <label class="control-label" for="LoginForm_password">Password <span class="required">*</span></label>
        <input class="form-control" name="LoginForm[password]" id="LoginForm_password" type="password"/>
        <?php
        if ($model->hasError('password')) {
            echo '<div class="alert alert-danger">' . $model->errors['password'] . '</div>';
        }
        ?>
    </div>

    <div class="form-group <?php echo $model->hasError('rememberMe') ? 'has-error' : ''; ?>">
        <input id="hidden_LoginForm_rememberMe" type="hidden" value="0" name="LoginForm[rememberMe]" />
        <span id="LoginForm_rememberMe">
            <div class="checkbox">
                <label>
                    <input id="LoginForm_rememberMe" value="1" type="checkbox" name="LoginForm[rememberMe]" />
                    Remember me next time
                </label>
            </div>
        </span>
    </div>

    <div class="form-group">
        <input class="btn btn-primary" type="submit" value="Login"></input>
    </div>
</form>