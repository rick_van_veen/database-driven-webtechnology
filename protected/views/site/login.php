<?php
/* @var $this SiteController */
?>

<div class="page-header">
    <h1>Sign in</h1>
</div>

<?php echo $this->renderPartial('_login_form', array('model' => $model)); ?>
