<?php
// Now you can access the categories and model here with the $model and $categories variables. See MVC pattern to know why.
?>

<form role="form" id ="input-form" class="col-lg-6" action="index.php?r=project/<?php echo $model->isNewRecord() ? 'create' : 'update/' . $model->id ?>" method="post">
    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <div class ="form-group <?php echo $model->hasError('title') ? 'has-error' : ''; ?>">
        <label class="control-label" for="Project_title">Project title <span class="required">*</span></label>
        <input name="Project[title]" type="text" class="form-control" id="Project_title" value="<?php echo $model->title ?>">
        <?php
        if ($model->hasError('title'))
            echo '<div class="alert alert-danger">' . $model->errors['title'] . '</div>';
        ?>
    </div>

    <div class="form-group <?php echo $model->hasError('category_id') ? 'has-error' : ''; ?>">
        <label class="control-label" for="Project_category_id">Category<span class="required">*</span></label>
        <select class="form-control" name="Project[category_id]" id="Project_category_id">
            <?php
            foreach ($categories as $categorie) {
                echo '<option value = "' . $categorie['id'] . '"> ' . $categorie['title'] . ' </option>';
            }
            ?>
        </select>
        <?php
        if ($model->hasError('category_id'))
            echo '<div class="alert alert-danger">' . $model->errors['category_id'] . '</div>';
        ?>
    </div>

    <div class="form-group <?php echo $model->hasError('description') ? 'has-error' : ''; ?>">
        <label class="control-label" for="Project_description">Description<span class="required">*</span></label>
        <textarea class="form-control" name ="Project[description]" rows="5" id="Project_description"><?php echo $model->description; ?></textarea>
        <?php
        if ($model->hasError('description'))
            echo '<div class="alert alert-danger">' . $model->errors['description'] . '</div>';
        ?>
    </div>

    <div class="form-group <?php echo $model->hasError('begin_date') ? 'has-error' : ''; ?>">
        <label class="control-label" for="Project_begin_date">Begin date<span class="required">*</span></label>
        <input class="form-control" type="date" name="Project[begin_date]" id="Project_begin_date" value="<?php echo $model->begin_date; ?>">
        <?php
        if ($model->hasError('begin_date'))
            echo '<div class="alert alert-danger">' . $model->errors['begin_date'] . '</div>';
        ?>
    </div>

    <div class="form-group <?php echo $model->hasError('end_date') ? 'has-error' : ''; ?>">
        <label class="control-label" for="Project_end_date">End Date<span class="required">*</span></label>
        <input class="form-control" type="date" id="end_date" name="Project[end_date]" value="<?php echo $model->end_date; ?>">
        <?php
        if ($model->hasError('end_date'))
            echo '<div class="alert alert-danger">' . $model->errors['end_date'] . '</div>';
        ?>
    </div>

    <div class="form-group">
        <input class="btn btn-primary" type="submit" value="<?php echo $model->isNewRecord() ? 'Create' : 'Save' ?>" />
    </div> 
</form>
