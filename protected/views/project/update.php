<?php
/* @var $thıs ProjectController */
?>

<div class="page-header">
    <h1>Update <?php echo '"' . $model->title . '"' ?></h1>
</div>

<?php
// The variables model and categories are the model and categories created in the controller. This is how render works. See XBaseController->render().
echo $this->renderPartial('_project_form', array('model' => $model, 'categories' => $categories));
?>
