
<a href="index.php?r=project/view/<?php echo $project['id'] ?>" class="list-group-item">
    <h4 class="list-group-item-heading"><?php echo $project['title']; ?></h4>
    <p class="list-group-item-text">
        <?php echo $project['description']; ?>
    </p>
</a>
