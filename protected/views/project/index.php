<div class="page-header">
    <h1>Explore Projects</h1>
</div>

<div class="list-group">
    <?php
    foreach ($projects as $project) {
        echo $this->renderPartial('_project_view', array('project' => $project));
    }
    ?>
</div>