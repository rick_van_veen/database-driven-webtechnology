<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/crowdsourcing.css">

        <title>The Big Idea</title>
    </head>

    <body>
        <div id="wrap">
            <header>
                <div class="container">
                    <img src="http://lorempixel.com/1048/200/technics/"/>
                    <nav class="navbar navbar-default" role="navigation">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="?r=site/index">The Big Idea</a>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <li><a href="index.php?r=project/create">Create project</a></li>
                                <li><a href="index.php?r=project/index">Explore projects</a></li>
                            </ul>
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="index.php?r=user/create">Sign up</a></li>
                                <?php
                                echo (XBase::app()->getUser()->isGuest()) ?
                                        '<li><a href = "index.php?r=site/login">Sign in</a></li>' :
                                        '<li><a href = "index.php?r=site/logout">Log out</a></li>';
                                ?>
                            </ul>
                        </div>
                    </nav>
                </div>
            </header>
            <section id="main_content">
                <div class="container">
                    <?php echo $content; ?>
                </div>
            </section>
        </div>
        <footer>
            <div class="container">
                <!--<h3> Some footer text </h3>-->
            </div>
        </footer>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>
