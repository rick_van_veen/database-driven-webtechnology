<?php

class UserController extends XBaseController {

    public function actionIndex() {
        
    }

    public function actionError() {
        
    }

    public function actionCreate() {
        $model = new User();
        if (isset($_POST['Register'])) {
            // Date is not set by user so needs to be done here.
            $datetime = date('Y-m-d h:i:s', time());
            $model->setAttributes(array('registration_date' => $datetime));

            $model->password_repeat = $_POST['Register']['password_repeat'];

            // Set all the user input in the model.
            $model->setAttributes($_POST['Register']);

            // Validate the model
            if ($model->validate()) {
                $model->password = XSecurityManager::hashPassword($model->password);
                // Save the model.
                try { // try catch sql errors and display error.
                    $model->save();
                } catch (Exception $e) {
                    $this->redirect('error'); // @todo need a better way to display this.
                }
            }
        }
        $this->render('create', array('model' => $model));
    }

}
