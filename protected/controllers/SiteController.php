<?php

class SiteController extends XBaseController {

    public function actionIndex() {
        $this->render('index');
    }

    public function actionError() {
        $this->render('error');
    }

    public function actionLogout() {
        XBase::app()->getUser()->logout();
        $this->redirect('site/index');
    }

    public function actionLogin() {
        $model = new LoginForm();
        //username, password, rememberMe
        if (isset($_POST['LoginForm'])) {
            $model->email = $_POST['LoginForm']['email'];
            $model->password = $_POST['LoginForm']['password'];
            $model->rememberMe = $_POST['LoginForm']['rememberMe'];

            if ($model->validate()) {
                $duration = $model->rememberMe * 3600 * 24;
                if ($model->login($duration)) {
                    $model->unsetAttributes();
                    $this->redirect('site/index');
                }
                else
                    $model->unsetAttributes();
            }
        }
        $this->render('login', array('model' => $model));
    }

}
