<?php

class ProjectController extends XBaseController {

    public function actionCreate() {

        if (XBase::app()->getUser()->isGuest()) {
            // @todo set some message maybe?
            $this->redirect('site/login');
        }

        $model = new Project();

        if (isset($_POST['Project'])) {
            $model->create_time = date('Y-m-d h:i:s', time());
            $model->owner_id = XBase::app()->getUser()->getId();
            $model->completed = 0;

            $model->setAttributes($_POST['Project']);

            if ($model->validate()) {
                try { // try catch sql errors and display error.
                    $model->save();
                } catch (Exception $e) {
                    $this->redirect('error'); // @todo need a better way to display this. Log the exception and display an error page.
                }
                $this->redirect('project/view/' . $model->id);
            }
        }

        // To get the categories in the view find them here and add a variable to the render function.
        $categories = $this->getCategories();

        $this->render('create', array('model' => $model, 'categories' => $categories));
    }

    private function getCategories() {
        $category = new Category();
        return $category->findAll(true);
    }

    public function actionError() {
        $this->render('error');
    }

    private function getProjects() {
        $project = new Project();
        return $project->findAll(true);
    }

    public function actionIndex() {
        $projects = $this->getProjects();
        $this->render('index', array('projects' => $projects));
    }

    public function actionView($id) {
        $project = new Project;
        $user = new User();
        if (isset($id)) { // @todo better error check here.
            $project->loadModel(intval($id[0]));
            $user->loadModel($project->owner_id);
        } else { // @todo change this.
            $this->render('error', array('message' => 'Nope!'));
        }
        $this->render('view', array('project' => $project, 'owner' => $user));
    }

    public function actionUpdate($id) {
        $project = new Project;

        if (isset($id)) {
            $project->loadModel(intval($id[0]));
        } else { // @todo change this.
            $this->render('error', array('message' => 'Nope!'));
        }

        if (!XBase::app()->getUser()->isGuest()) {
            if (!XBase::app()->getUser()->isOwner($project->owner_id)) {
                $this->redirect('project/error');
            }
        } else {
            $this->redirect('site/login');
        }

        if (isset($_POST['Project'])) {
            $project->setAttributes($_POST['Project']);

            if ($project->validate()) {
                $project->save();
            }
        }

        $categories = $this->getCategories();
        $this->render('update', array('model' => $project, 'categories' => $categories));
    }

}
